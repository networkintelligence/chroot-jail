# Chroot-Jail
This Shell script can create a chrooted environment along with a SSH Jail for the same. This can be used either for a single user jail or to create a chroot jailed group.

![alt text](https://bytebucket.org/networkintelligence/chroot-jail/raw/f731690984b00bf78d7ed151f58c0423b7d49d9a/HowTo.gif)
